<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Session;
use GeneralHelper;

class RecipesController extends Controller
{
    public function __construct()
    {

    }

    public function create()
    {
        $categories = Category::orderBy('name', 'asc')->get();
        return view('recipes.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'ingredients' => 'required',
            'description' => 'required',
//            'photo' => 'image',
            'categories' => 'required',
            'categories.*' => 'exists:categories,id'
        ]);

        $photoName = null;
        if ($request->hasFile('photo')) {
            $photoName = GeneralHelper::uploadFile($request->file('photo'), null, public_path('uploads/images'));
        }

        $recipe = Recipe::create(
            [
                'name' => $request->name,
                'ingredients' => $request->ingredients,
                'description' => $request->description,
                'user_id' => auth()->id(),
                'photo' => $photoName
            ]
        );

        $recipe->categories()->sync($request->categories);

        Session::flash('success', 'Recipe was added. Wait for admin confirmation.');
        return redirect()->route('home.index');
    }
}

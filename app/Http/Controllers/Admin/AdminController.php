<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.admin.index');
    }
}

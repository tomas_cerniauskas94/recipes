<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class CategoriesController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $categories = Category::orderBy('name', 'asc')->paginate(10);
        return view('admin.categories.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories,name'
        ]);

        Category::create([
            'name' => $request->name
        ]);

        Session::flash('success', 'New category was added.');
        return redirect()->route('admin.categories.index');
    }

    public function edit($categoryId)
    {
        $category = Category::find($categoryId);
        if (!$category) {
            Session::flash('error', 'Kategorija nerasta.');
            return redirect()->back();
        }

        return view('admin.categories.edit', compact('category'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:categories,id',
            'name' => 'required|unique:categories,name,' . $request->category_id
        ]);

        $category = Category::find($request->category_id);
        $category->update(['name' => $request->name]);

        Session::flash('success', 'Category was edited.');
        return redirect()->route('admin.categories.index');
    }

    public function delete(Request $request)
    {
        $category = Category::find($request->category_id);
        if($category){
            $category->recipes()->detach();
            $category->delete();
        }
        Session::flash('success', 'Category was deleted.');
        return redirect()->route('admin.categories.index');
    }
}

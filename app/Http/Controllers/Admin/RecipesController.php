<?php

namespace App\Http\Controllers\Admin;

use App\Models\Recipe;
use App\Repositories\RecipesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class RecipesController extends Controller
{
    public function index()
    {
        $recipes = Recipe::orderBy('is_confirmed', 'asc')->orderBy('name', 'asc')->paginate(10);

        return view('admin.recipes.index', compact('recipes'));
    }

    public function confirmRecipe(Request $request)
    {
        print_r($request->all());
    }
}

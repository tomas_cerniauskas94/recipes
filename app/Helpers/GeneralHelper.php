<?php

namespace App\Helpers;

class GeneralHelper
{
    public static function uploadFile($uploadedFile, $name = null, $path)
    {
        $file = $uploadedFile;
        if (empty($name)) {
            $name = uniqid() . '.' . $file->getClientOriginalExtension();
        }
        $file->move($path, $name);

        return $name;
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $table = 'recipes';
    protected $fillable = [
        'name', 'ingredients', 'description', 'photo', 'user_id', 'is_confirmed'
    ];

    const CONFIRMED_TRUE = 1;
    const CONFIRMED_FALSE = 0;

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category', 'recipes_categories', 'recipe_id', 'category_id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $fillable = [
        'name'
    ];

    public function recipes()
    {
        return $this->belongsToMany('App\Models\Recipe', 'recipes_categories', 'category_id', 'recipe_id');
    }
}

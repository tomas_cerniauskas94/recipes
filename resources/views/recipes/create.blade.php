@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-xs-12">
                    <div class="card text-white bg-dark">
                        <div class="card-header">Create Recipe</div>
                        <div class="card-body">
                            <form action="{{ route('recipes.store') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Title">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="ingredients">Ingredients</label>
                                    <textarea id="ingredients" name="ingredients" placeholder="Ingredients" class="form-control">{{ old('ingredients') }}</textarea>
                                    @if($errors->has('ingredients'))
                                        <span class="text-danger">{{ $errors->first('ingredients') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea id="description" name="description" placeholder="Description" class="form-control">{{ old('description') }}</textarea>
                                    @if($errors->has('description'))
                                        <span class="text-danger">{{ $errors->first('description') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="categories">Categories</label>
                                    <select id="categories" name="categories[]" class="form-control select2" data-placeholder="Category" multiple="multiple">
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}" {{ in_array($category->id, is_array(old('categories')) ? old('categories'):[]) ? 'selected':'' }}>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has('categories'))
                                        <span class="text-danger">{{ $errors->first('categories') }}</span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="photo">Photo</label>
                                    <input type="file" name="photo" id="photo">
                                    @if($errors->has('photo'))
                                        <div class="text-danger">{{ $errors->first('photo') }}</div>
                                    @endif
                                </div>
                                <button class="btn btn-success btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
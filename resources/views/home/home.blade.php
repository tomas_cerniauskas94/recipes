@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <h1 class="text-center">Welcome</h1>
            @if(auth()->check())
                <div class="row">
                    <div class="col-md-12">
                        <a class="btn btn-dark float-right" href="{{ route('recipes.create') }}">New Recipe</a>
                    </div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-8 col-md-offset-2">

                </div>
            </div>
        </div>
    </section>
@endsection
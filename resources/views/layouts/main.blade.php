<html>
<head>
    <title>Recipes</title>
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-4.1.2/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script>window.laravel = '{{ csrf_token() }}';</script>
    <script src="{{ asset('assets/plugins/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-4.1.2/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    @yield('top_scripts')
</head>
<body class="bg-light">
    @include('layouts.includes._navbar')
    @if(Session::has('success'))
        <div class="row mt-3">
            <div class="col-md-8 offset-md-2 col-xs-12">
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            </div>
        </div>
    @endif
    @if(Session::has('error'))
        <div class="row mt-3">
            <div class="col-md-8 offset-md-2 col-xs-12">
                <div class="alert alert-danger">
                    {{ Session::get('error') }}
                </div>
            </div>
        </div>
    @endif
    @yield('content')
    <script src="{{ asset('assets/js/common.js') }}"></script>
    @yield('bottom_scripts')
</body>
</html>
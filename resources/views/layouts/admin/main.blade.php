<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Recipes</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-4.1.2/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <script>window.laravel = '{{ csrf_token() }}';</script>
    <script src="{{ asset('assets/plugins/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-4.1.2/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>
    @yield('top_scripts')
</head>
<body class="bg-light">
<div id="admin-layout">
    <div class="page-container">
        <div class="side-nav">
            <div class="title">
                <h3>RECIPES ADMIN</h3>
            </div>
            <ul class="menu-items">
                <li class="has-sub item">
                    <a href="{{ route('admin.categories.index') }}">Categories</a>
                    <ul class="sub-menu-items">
                        <li class="sub-menu-item"><a href="{{ route('admin.recipes.index') }}">All categories</a></li>
                        <li class="sub-menu-item"><a href="{{ route('admin.recipes.index') }}">New Category</a></li>
                    </ul>
                </li>
                <li class="item"><a href="{{ route('admin.recipes.index') }}">Recipes</a></li>
            </ul>
        </div>
        <div class="main-content">
            @if(Session::has('success'))
                <div class="row mt-3">
                    <div class="col-md-8 offset-md-2 col-xs-12">
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    </div>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="row mt-3">
                    <div class="col-md-8 offset-md-2 col-xs-12">
                        <div class="alert alert-danger">
                            {{ Session::get('error') }}
                        </div>
                    </div>
                </div>
            @endif
            <nav class="top-nav">
                <ul class="menu-items right">
                    <li class="has-sub menu-item">
                        <a href="#">{{ auth()->user()->name }} <span class="fa fa-angle-down"></span></a>
                        <ul class="sub-menu-items">
                            <li class="seperator"></li>
                            <li class="sub-menu-item"><a href="#">Atsijungti</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            @yield('content')
        </div>
    </div>
</div>
<script src="{{ asset('assets/js/common.js') }}"></script>
<script src="{{ asset('assets/js/admin-common.js') }}"></script>
@yield('bottom_scripts')
</body>
</html>
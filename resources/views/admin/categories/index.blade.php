@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-sm-12 col-xs-12">
                    <h3>Categories</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered bg-white">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category name</th>
                                    <th>Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->id }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            <a href="{{ route('admin.categories.edit', ['categoryId' => $category->id]) }}"><span class="fa fa-pencil"></span></a>
                                            <form id="category-delete-form-{{ $category->id }}" action="{{ route('admin.categories.delete') }}" method="post" style="display: inline-block; margin: 0;">
                                                @csrf
                                                <input type="hidden" name="category_id" value="{{ $category->id }}">
                                                <a class="text-danger" data-type="submit-delete-form" data-form="#category-delete-form-{{ $category->id }}" href=""><span class="fa fa-trash"></span></a>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $categories->links() }}
                </div>
                <div class="col-md-2">
                    <div class="card text-white bg-dark">
                        <div class="card-header">Create Category</div>
                        <div class="card-body">
                            <form action="{{ route('admin.categories.store') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Category name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="Title">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <button class="btn btn-success btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
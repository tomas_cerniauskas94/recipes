@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-xs-12">
                    <div class="card text-white bg-dark">
                        <div class="card-header">Edit Category <strong>{{ $category->name }}</strong></div>
                        <div class="card-body">
                            <form action="{{ route('admin.categories.update') }}" method="post">
                                @csrf
                                <input type="hidden" name="category_id" value="{{ $category->id }}">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}" placeholder="Title">
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <button class="btn btn-success btn-block">Save</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
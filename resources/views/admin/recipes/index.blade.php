@extends('layouts.main')

@section('content')
    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 col-sm-12 col-xs-12">
                    <h3>Recipes</h3>
                    <div class="table-responsive">
                        <table class="table table-bordered bg-white">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Is confirmed</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($recipes as $recipe)
                                    <tr>
                                        <td>{{ $recipe->id }}</td>
                                        <td>{{ $recipe->name }}</td>
                                        <td>
                                            <div class="input-group-sm">
                                                <select data-type="recipe-confirmation" data-recipe-id="{{ $recipe->id }}" name="recipe_id" class="form-control">
                                                    <option value="{{ \App\Models\Recipe::CONFIRMED_FALSE }}">Unconfirmed</option>
                                                    <option value="{{ \App\Models\Recipe::CONFIRMED_TRUE  }}" {{ $recipe->is_confirmed ? 'selected':'' }}>Confirmed</option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $recipes->links() }}
                </div>
            </div>
        </div>
        <input type="hidden" id="confirm_recipe_url" name="confirm_recipe_url" value="{{ route('admin.recipes.confirmRecipe') }}">
    </section>
@endsection

@section('bottom_scripts')
    <script src="{{ asset('assets/js/admin-recipes.js') }}"></script>
@endsection
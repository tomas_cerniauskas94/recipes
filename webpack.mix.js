let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('public/assets/css/scss/style.scss', 'public/css');
mix.sass('public/assets/css/scss/admin.scss', 'public/css');
mix.js('public/assets/js/common.js', 'public/js');
mix.js('public/assets/js/admin-recipes.js', 'public/js');
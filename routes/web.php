<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home.index');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/recipes/create', 'RecipesController@create')->name('recipes.create');
    Route::post('/recipes/store', 'RecipesController@store')->name('recipes.store');
});

Route::group(['middleware' => ['admin']], function () {
    Route::get('/admin/categories/index', 'Admin\CategoriesController@index')->name('admin.categories.index');
    Route::post('/admin/categories/store', 'Admin\CategoriesController@store')->name('admin.categories.store');
    Route::get('/admin/categories/edit/{categoryId}', 'Admin\CategoriesController@edit')->name('admin.categories.edit');
    Route::post('/admin/categories/update', 'Admin\CategoriesController@update')->name('admin.categories.update');
    Route::post('/admin/categories/delete', 'Admin\CategoriesController@delete')->name('admin.categories.delete');

    Route::get('/admin/recipes/index', 'Admin\RecipesController@index')->name('admin.recipes.index');
    Route::post('/admin/recipes/confirm', 'Admin\RecipesController@confirmRecipe')->name('admin.recipes.confirmRecipe');

    Route::get('/admin', 'Admin\AdminController@index')->name('admin.admin.index');
});

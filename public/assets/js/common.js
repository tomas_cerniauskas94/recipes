$('[data-type=submit-delete-form]').click(function () {
    if (confirm('Do you want to delete?')) {
        $($(this).data('form')).submit();
    }
    return false;
});

$('.select2').select2();

function addLoading(element, append, uniqueId) {
    var element = $(element);
    var loader = '<div id="' + uniqueId + '" class="loader loader-sm"></div>';
    if (append) {
        element.append(loader);
    } else {
        element.after(loader);
    }
}
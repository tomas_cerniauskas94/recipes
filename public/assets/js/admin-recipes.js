var confirmRecipeUrl = $('#confirm_recipe_url').val();

$('select[data-type=recipe-confirmation]').change(function(e){
    var recipeId = $(this).data('recipe-id');
    var isConfirmed = $(this).val();
    var selectBox = $(this);

    selectBox.hide();
    addLoading(selectBox, false, 'loading-'+recipeId);

    $.post(confirmRecipeUrl, {
        recipe_id:recipeId,
        is_confirmed:isConfirmed,
        _token: window.laravel
    }, function(data){
        $('#loading-'+recipeId).remove();
        selectBox.show();
    });
});